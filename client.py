import socket
import ssl
import base64
from getpass import getpass
import re


def send_recv(command, sock):
    sock.send((command + '\n').encode())
    result = b''
    while True:
        try:
            data = sock.recv(1024)
            result += data
        except:
            break
    return result.decode()


def send_recv_oneline(data, sock):
    data = data.encode()
    data += b'\n'
    sock.send(data)
    return sock.recv(1024).decode(encoding='utf-8')


def find_attachments(message, boundary):
    parts_of_msg = message.split(boundary)
    for part in parts_of_msg:
        all_content = re.search('(?<=Content-Type: )(.*)(?=;)(.*)(?=--)', part, flags=re.DOTALL)
        subject = re.search('(?<=Subject: )(\s*)(.*)', part)
        if subject is not None:
            print(f'Subject: {subject.group(0)}')
        if all_content is not None:
            content_type = all_content.group(1)
            if content_type == 'text/plain' and all_content.group(2).startswith('; charset'):
                headers = ''.join(all_content.group(2).split('\n')[0:5])
                text = all_content.group(2)[len(headers):]
                print(f'Text: \n\n{text}\n')
            content = re.search('(?<=filename=")(\S*)(?=")(.*)(?<=")(\s*)(.*)(?=--)', part, flags=re.DOTALL)
            if content is not None:
                filename = content.group(1)
                ext = content_type.split('/')[1]
                if content_type == 'text/plain':
                    ext = 'txt'
                if ext in ['png', 'jpg', 'txt']:
                    save_attachment(filename, content.group(4))


def save_attachment(filename, content):
    decoded = base64.b64decode(content)
    with open(filename, 'wb+') as file:
        file.write(decoded)
        print(f'Found attachment, saved to {filename}\n')


def print_message_text(message):
    subject = re.search('(?<=Subject: )(\s*)(.*)', message).group(0)
    text = re.search('(?<=X-Yandex-Forward: .{32})(\s*)(.*)', message, flags=re.DOTALL).group(2)[:-4]
    print('No attachments found.')
    print(f'Subject: {subject}')
    print(f'Text: \n\n{text}\n')


def main():
    pop3_host = ('pop.yandex.ru', 995)

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock = ssl.wrap_socket(sock)
            sock.settimeout(1)
            sock.connect(pop3_host)
            print(sock.recv(1024).decode())
            print(f'connected successfully to {pop3_host[0]}')

            login = input('Enter your login for yandex mail service: ')
            print(send_recv_oneline(f'USER {login}', sock))
            password = getpass('Enter password: ')
            password_answer = send_recv_oneline(f'PASS {password}', sock)
            if not password_answer.startswith('+OK'):
                exit(1)
            print(password_answer)
            info = ('Available commands: '
                    '\n\tHELP - print this info'
                    '\n\tSTAT - get number of messages and size of mailbox in octets'
                    '\n\tLIST [msg_id] - print info about all messages or specified one'
                    '\n\tDELE msg_id - mark message for removing'
                    '\n\tRETR msg_id - get message'
                    '\n\tTOP msg_id lines_count - get specified message or first lines_count lines of message with headers'
                    '\n\tRSET - decline all changes in current transaction'
                    '\n\tNOOP - do nothing'
                    '\n\tQUIT - apply changes, close transactions and exit program\n')
            print(info)

            while True:
                command = input('>>> ').upper()
                if command == 'QUIT':
                    print(send_recv_oneline(command, sock))
                    break
                if command == 'HELP':
                    print(info)
                    continue
                if command[:4] in ['STAT', 'RSET', 'NOOP', 'DELE']:
                    print(send_recv_oneline(command, sock))
                    continue
                elif command[:4] not in ['LIST', 'RETR', 'TOP ']:
                    print('Invalid input\n')
                    continue

                if command[:4] == 'RETR':
                    message = send_recv(command, sock)
                    try:
                        boundary = re.search('(?<=Content-Type: multipart/mixed; boundary=")(.*)(?=")', message)
                        if boundary is None:
                            print_message_text(message)
                            continue
                        find_attachments(message, boundary.group(1))

                    except AttributeError:
                        continue

                    continue

                print(send_recv(command, sock))
                continue
    except SystemExit:
        print('Incorrect password')
    except:
        print('Something wrong with connection to the internet.')


if __name__ == '__main__':
    main()
